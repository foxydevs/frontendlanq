import { Component, OnInit } from '@angular/core';

import { NotificationsService } from 'angular2-notifications';

declare var $: any

@Component({
  selector: 'app-municipios',
  templateUrl: './municipios.component.html',
  styleUrls: ['./municipios.component.css']
})
export class MunicipiosComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    setTimeout(function(){$(".dropdown-toggle").dropdown('toggle');},100)
  }

}
