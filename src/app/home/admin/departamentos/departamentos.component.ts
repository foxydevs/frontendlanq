import { Component, OnInit } from '@angular/core';

import { NotificationsService } from 'angular2-notifications';

declare var $: any

@Component({
  selector: 'app-departamentos',
  templateUrl: './departamentos.component.html',
  styleUrls: ['./departamentos.component.css']
})
export class DepartamentosComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    setTimeout(function(){$(".dropdown-toggle").dropdown('toggle');},100)
  }

}
