import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from "./dashboard/dashboard.component";
import { AdminComponent } from "./admin.component";
import { UsuariosComponent } from './usuarios/usuarios.component';
import { RolesComponent } from './roles/roles.component';
import { SucursalesComponent } from './sucursales/sucursales.component';
import { PuestosComponent } from './puestos/puestos.component';
import { ClientesComponent } from './clientes/clientes.component';
import { PerfilComponent } from './perfil/perfil.component';
import { VentasComponent } from './ventas/ventas.component';
import { ModulosComponent } from './modulos/modulos.component';
import { EstadisticasComponent } from './estadisticas/estadisticas.component';
import { MetasComponent } from './metas/metas.component';
import { CitasComponent } from './citas/citas.component';
import { PedidosComponent } from './pedidos/pedidos.component';
import { QuejasComponent } from './quejas/quejas.component';
import { EventosComponent } from './eventos/eventos.component';
import { VendedorComponent } from './vendedor/vendedor.component';
import { DepartamentosComponent } from './departamentos/departamentos.component';
import { MunicipiosComponent } from './municipios/municipios.component';
const routes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  { path: '', component: AdminComponent, children: [
    { path: 'dashboard', component: DashboardComponent },
    { path: 'usuarios', component: UsuariosComponent },
    { path: 'roles', component: RolesComponent },
    { path: 'sucursales', component: SucursalesComponent },
    { path: 'puestos', component: PuestosComponent },
    { path: 'clientes', component: ClientesComponent },
    { path: 'perfil', component: PerfilComponent },
    { path: 'ventas', component: VentasComponent },
    { path: 'modulos', component: ModulosComponent },
    { path: 'estadistica', component: EstadisticasComponent },
    { path: 'metas', component: MetasComponent },
    { path: 'citas', component: CitasComponent },
    { path: 'pedidos', component: PedidosComponent },
    { path: 'quejas', component: QuejasComponent },
    { path: 'eventos', component: EventosComponent },
    { path: 'vendedor', component: VendedorComponent },
    { path: 'departamentos', component: DepartamentosComponent },
    { path: 'municipios', component: MunicipiosComponent },
  ]},
  { path: '**', redirectTo: 'dashboard', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
