import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NavComponent } from "./nav.component";
import { VendedorGuard } from "./../_guards/vendedor.guard";
import { ClienteGuard } from "./../_guards/cliente.guard";
import { AdminGuard } from "./../_guards/admin.guard";

const routes: Routes = [
  { path: '', redirectTo: 'admin', pathMatch: 'full' },
  { path: '', component: NavComponent, children: [
    { path: 'vendedor',loadChildren: 'app/home/vendedor/vendedor.module#VendedorModule', canActivate: [VendedorGuard]},
    { path: 'cliente',loadChildren: 'app/home/cliente/cliente.module#ClienteModule', canActivate: [ClienteGuard]},
    { path: 'admin',loadChildren: 'app/home/admin/admin.module#AdminModule', canActivate: [AdminGuard]},
  ]},
  { path: '**', redirectTo: '', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
