import { TestBed, async, inject } from '@angular/core/testing';

import { VendedorGuard } from './vendedor.guard';

describe('VendedorGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [VendedorGuard]
    });
  });

  it('should ...', inject([VendedorGuard], (guard: VendedorGuard) => {
    expect(guard).toBeTruthy();
  }));
});
